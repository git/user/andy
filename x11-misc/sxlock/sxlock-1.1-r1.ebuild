# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils

DESCRIPTION="Simple screen locker utility for X, fork of sflock, which is based on slock."
HOMEPAGE="https://github.com/lahwaacz/sxlock"
SRC_URI="https://github.com/lahwaacz/sxlock/archive/v${PV}.tar.gz -> ${P}.tar.gz"
PATCHES=( "${FILESDIR}/${P}-red-background.patch" )

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXrandr
	sys-libs/pam"
RDEPEND="${DEPEND}"

