# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools

DESCRIPTION="Desktop Applications Autostarter"
HOMEPAGE="https://jjacky.com/dapper/"
SRC_URI="https://github.com/jjk-jacky/${PN}/archive/${PV/_/}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	default

	# Remove problematic CFLAGS declaration
	sed -i -e '/^AM_CFLAGS/s: -g : :' Makefile.am || die

	# Rerun autotools
	mkdir -p m4
	einfo "Regenerating autotools files..."
	eaclocal -I m4 && eautoheader || die "Fail"
	WANT_AUTOCONF=2.69 eautoconf
	mkdir -p build-aux
	eautomake
}
