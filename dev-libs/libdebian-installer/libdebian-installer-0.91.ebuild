# Copyright 2009 Ricardo Salveti de Araujo
# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit autotools

DESCRIPTION="Library of common debian-installer functions"
HOMEPAGE="http://packages.qa.debian.org/libd/libdebian-installer.html"
SRC_URI="mirror://debian/pool/main/libd/${PN}/${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc"

DEPEND="sys-devel/libtool
		dev-util/pkgconfig
		doc? ( app-doc/doxygen )"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}"

src_prepare() {
    # Remove problematic LDFLAGS declaration
    sed -i -e '/^LDFLAGS/d' src/Makefile.am || die

    # Rerun autotools
    einfo "Regenerating autotools files..."
	eautoreconf
}

src_compile() {
	emake || die "emake failed"

	if use doc; then
		emake -C doc doc || die "emake for docs failed"
	fi
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	if use doc; then
		dohtml -r doc/html/*
	fi

	dodoc HACKING debian/changelog 
}
