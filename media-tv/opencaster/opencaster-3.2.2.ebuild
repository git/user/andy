# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python2_6 python2_7 )

inherit eutils distutils-r1

DESCRIPTION="MPEG2 transport stream data generator and packet manipulator"
HOMEPAGE="http://www.avalpa.com/the-key-values"
SRC_URI="mirror://debian/pool/main/o/opencaster/${P/-/_}+dfsg.orig.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}/${P}-patch-tun-path.patch"
	cd libs/dvbobjects && distutils-r1_src_prepare
}

src_compile() {
	default
	cd libs/dvbobjects && distutils-r1_src_compile
}

src_install() {
	mkdir -p "${D}usr/bin/"
	emake DESTDIR="${D}usr/bin/" INSTHOME=${D}usr install

	for d in README CHANGES ; do
		[[ -s "${d}" ]] && dodoc "${d}"
	done
	
	cd libs/dvbobjects && distutils-r1_src_install
}

