# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Bootstrap a Debian system"
HOMEPAGE="http://packages.qa.debian.org/c/cdebootstrap.html"
SRC_URI="mirror://debian/pool/main/c/${PN}/${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="net-misc/wget
		app-arch/xz-utils
		sys-libs/zlib
		dev-libs/libdebian-installer"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}"
